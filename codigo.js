var ctx;
var larg, alt;
var t, ha_movimento;
var vel, ang;
var x1, y1, x2, y2; 		//Coordenadas dos projeteis
var xP1, yP1, xP2, yP2;		//Coordenadas dos players
var x0P1, y0P1, x0P2, y0P2, acel;
var oldAng;
var turn;
var victoryP1;
var victoryP2;
var randomHeight;
var level;
var xRato, yRato, xRatoD, yRatoD; //Cursor
var distancia;
var pontos1, pontos2;
var pontosT1, pontosT2;

/*	Imagens */
var playerOne = new Image();
playerOne.src = "box_green.png";

var playerTwo = new Image();
playerTwo.src = "box_red.png";

var pOProjetil = new Image();
pOProjetil.src = "ball_green.png";

var pTProjetil = new Image();
pTProjetil.src = "ball_red.png";

var background = new Image();
background.src = "background.png";

var mainmenu = new Image();
mainmenu.src = "mainmenu.png";

var creditsButton = new Image();
creditsButton.src = "credits.png"

var playButton = new Image();
playButton.src = "play.png"
/************/

function init() {
    var canvas = document.getElementById("cvs");
    ctx = canvas.getContext("2d");
    larg = canvas.getAttribute("width");
    alt = canvas.getAttribute("height");
    document.addEventListener("mousemove", moveRato);
    pontos1 = pontos2 = 0;
    pontosT1 = pontosT2 = 0;
    level = 2;
    Level1();
    loop();
}

function loop() {
    window.requestAnimationFrame(loop);
    update();
    render();
}

function update() {


    if (level != 0) {
        calcDistancia();
        if (turn == 2) {
            ang = Math.atan2(yRato - (y0P2 + playerTwo.height / 2), xRato - (x0P2 + playerTwo.width / 2));
            if (ha_movimento && (turn == 2)) {
                t++;
                x2 = x0P2 + vel * Math.cos(ang) * t;
                y2 = y0P2 - vel * Math.sin(-ang) * t + acel / 2 * t * t;
            }
        } else if (turn == 1) {
            ang = Math.atan2(yRato - (y0P1 + playerOne.height / 2), xRato - (x0P1 + playerOne.width / 2));
            if (ha_movimento && (turn == 1)) {
                t++;
                x1 = x0P1 + vel * Math.cos(ang) * t;
                y1 = y0P1 - vel * Math.sin(-ang) * t + acel / 2 * t * t;
            }
        } else {
            Turns();
        }


        /*	Colisões dos projeteis com os jogadores */
        if (x1 > xP2 && x1 < xP2 + 50 && y1 > yP2 && y1 < yP2 + 50) {
            ha_movimento = false;
            t = 0;
            ang = oldAng;
            turn = 2;
            x1 = 43;
            y1 = yP1 + 10;
            pontos1++;
            if (pontos1 == 3 && level == 1) {
                pontosT1++;
                pontos1 = 0;
                Level2();
            }
        }

        if (x2 > xP1 && x2 < xP1 + 50 && y2 > yP1 && y2 < yP1 + 50) {
            ha_movimento = false;
            t = 0;
            ang = oldAng;
            turn = 1;
            x2 = 1110;
            y2 = yP2 + 10;
            pontos2++;
            if (pontos2 == 3 && level == 1) {
                pontosT2++;
                pontos2 = 0;
                Level2();
            }
        }

        /*	Colisão com os obstaculos 
    
            if(x1 > larg/2 && x1 < larg/2 + 20 && y1 > randomHeight && y1 < randomHeight + 200) {
                ha_movimento = false;
                t = 0;
                ang = oldAng;
                turn = 2;
                x1 = 43;
                y1 = yP1 + 10;
            }
        	
            if(x2 > larg/2 && x2 < larg/2 + 20 && y2 > randomHeight && y2 < randomHeight + 200) {
                ha_movimento = false;
                t = 0;
                ang = oldAng;
                turn = 1;
                x2 = 1110;
                y2 = yP2 + 10;
            }*/

        /*	Se o projetil passar da altura do canvas para baixo é feito o reset dos mesmos */
        if (y1 > alt) {
            ha_movimento = false;
            t = 0;
            ang = oldAng;
            turn = 2;
            x1 = 43;
            y1 = yP1 + 10;
        } else if (y2 > alt) {
            ha_movimento = false;
            t = 0;
            ang = oldAng;
            turn = 1;
            x2 = 1110;
            y2 = yP2 + 10;
        }
    }
}

function render() {

    ctx.clearRect(0, 0, larg, alt);

    /*	Em caso de vitoria */
    if (victoryP1 && !victoryP2) {
        ctx.fillStyle = "white";
        ctx.font = "60px Arial";
        ctx.fillText("PLAYER 1 WINS!", 350, 400);
    }
    if (victoryP2 && !victoryP1) {
        ctx.fillStyle = "white";
        ctx.font = "60px Arial";
        ctx.fillText("PLAYER 2 WINS!", 350, 400);
    }

    //Background
    ctx.drawImage(background, 0, 0)

    //Player one projetil
    ctx.drawImage(pOProjetil, x1, y1);

    //Player two projetil
    ctx.drawImage(pTProjetil, x2, y2);

    //Player one body
    ctx.drawImage(playerOne, xP1, yP1);

    //Platform P1
    ctx.fillStyle = "grey";
    ctx.beginPath();
    ctx.rect(xP1 - 20, yP1 + 50, 80, 20);
    ctx.fill();

    //Player two body
    ctx.drawImage(playerTwo, xP2, yP2);

    //Platform P2
    ctx.fillStyle = "grey";
    ctx.beginPath();
    ctx.rect(xP2 - 10, yP2 + 50, 80, 20);
    ctx.fill();


    /*	Mostra ao jogador em tempo real o angulo e velocidade para saber para onde etá a apontar */
    ctx.fillStyle = "white";
    ctx.textAlign = "center"
    ctx.font = "20px Arial";
    ctx.fillText("Velocidade: " + vel, cvs.width / 2, 660);

    // Desenhar valor do angulo
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    if (ha_movimento) {
        ctx.fillText("Ângulo: " + -oldAng * 180 / Math.PI, cvs.width / 2, 680);
    } else {
        ctx.fillText("Ângulo: " + -ang * 180 / Math.PI, cvs.width / 2, 680);
    }


    // Desenhar Turno
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    if (turn == 1) {
        ctx.fillText("Turno de: Jogador Verde", cvs.width / 2, 640);
    } else {
        ctx.fillText("Turno de: Jogador Vermelho", cvs.width / 2, 640);
    }

    // Desenhar Numero do Nivel
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    ctx.fillText("Nível: " + level, cvs.width / 2, 620);

    // Desenhar Pontos do Jogador 1
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    ctx.fillText("Pontos: " + pontos1, 50, 660);

    // Desenhar Pontos do Jogador 2
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    ctx.fillText("Pontos: " + pontos2, 1150, 660);

    // Desenhar Pontos do Jogador 1
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    ctx.fillText("Níveis ganhos: " + pontosT1, 82, 680);

    // Desenhar Pontos do Jogador 2
    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    ctx.fillText("Níveis ganhos: " + pontosT2, 1118, 680);

    /* 	Desenhar Cursor */
    if (turn == 1) {
        ctx.fillStyle = "green";
    } else if (turn == 2) {
        ctx.fillStyle = "red";
    }
    ctx.beginPath();
    ctx.arc(xRatoD, yRatoD, 5, 0, 2 * Math.PI);
    ctx.fill();
}

function moveRato(e) {
    if (!ha_movimento) {
        xRato = e.pageX - cvs.offsetLeft;
        yRato = e.pageY - cvs.offsetTop;
    }
    xRatoD = e.pageX - cvs.offsetLeft;
    yRatoD = e.pageY - cvs.offsetTop;
}

function calcDistancia() {

    var a;
    var b;
    if (turn == 1) {
        a = xRato - x0P1;
        b = yRato - y0P1;

        distancia = Math.sqrt(a * a + b * b);
        vel = distancia / 30;
    } else if (turn == 2) {
        a = xRato - x0P2;
        b = yRato - y0P2;

        distancia = Math.sqrt(a * a + b * b);
        vel = distancia / 30;
    }
}

function Iniciar() {
    //vel = document.getElementById("velocidade").value;
    if (ha_movimento || victoryP1 || victoryP2) {
        return;
    }
    oldAng = ang;
    ang = ang * Math.PI / 180;
    t = 0;
    ha_movimento = true;
}

function Turns() {
    if (turn == null) {
        turn = 1;
    } else if (turn == 1) {
        turn = 2;
    } else if (turn == 2) {
        turn = 1;
    }
}

function MainMenu() {

    //Menu background
    ctx.drawImage(mainmenu, 0, 0);

    if (xRatoD > 100 && xRatoD < 465 && yRatoD > 275 && yRatoD < 450) {
        ctx.strokeStyle = "#55FFE1";
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.strokeRect(100, 295, 350, 135);
        ctx.stroke();
        ctx.drawImage(playButton, 0, 0);
        document.addEventListener("mouseup", Level1);
    } else if (xRatoD > 100 && xRatoD < 670 && yRatoD > 450 && yRatoD < 575) {
        ctx.strokeStyle = "#55FFE1";
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.strokeRect(100, 445, 575, 135);
        ctx.stroke();
        ctx.drawImage(creditsButton, 0, 0);
    }

}

function Level1() {

    acel = 0.5;
    t = 0;
    ha_movimento = false;
    victoryP2 = victoryP1 = false;
    vel = 10;
    ang = 45;

    //Player One
    xP1 = 40;
    x1 = xP1 + 3;
    yP1 = Math.floor((Math.random() * 500) + 100);
    y1 = yP1 + 10;

    //Player Two
    xP2 = 1100;
    x2 = xP2 + 10;
    yP2 = Math.floor((Math.random() * 500) + 100);
    y2 = yP2 + 10;
    randomHeight = Math.floor((Math.random() * 300) + 100);


    x0P1 = 43;
    y0P1 = yP1 + 10;
    x0P2 = 1110;
    y0P2 = yP2 + 10;

    level = 1;


}

function Level2() {

    acel = 0.5;
    t = 0;
    ha_movimento = false;
    victoryP2 = victoryP1 = false;
    vel = 10;
    ang = 45;

    //Player One
    xP1 = 40;
    x1 = xP1 + 3;
    yP1 = Math.floor((Math.random() * 500) + 100);
    y1 = yP1 + 10;

    //Player Two
    xP2 = 1100;
    x2 = xP2 + 10;
    yP2 = Math.floor((Math.random() * 500) + 100);
    y2 = yP2 + 10;
    randomHeight = Math.floor((Math.random() * 300) + 100);


    x0P1 = 43;
    y0P1 = yP1 + 10;
    x0P2 = 1110;
    y0P2 = yP2 + 10;

    level = 2;
}



